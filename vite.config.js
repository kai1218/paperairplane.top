import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
import viteCompression from 'vite-plugin-compression'
const productionGzipExtensions = /\.(js|css|json|png|jpg|txt|html|ico|svg)(\?.*)?$/i

export default defineConfig({
  plugins: [
    vue(),
    AutoImport({
      resolvers: [ElementPlusResolver()],
    }),
    Components({
      resolvers: [ElementPlusResolver()],
    }),
    viteCompression({
      threshold: 10240,
      filter: /\.(js|css|json|png|jpg|txt|html|ico|svg)(\?.*)?$/i
    })
  ]
})
