import { createRouter, createWebHistory } from 'vue-router'
import IndexView from '../views/IndexView.vue'
import http from '../assets/js/http'

const routes = [
  {
    path: '/',
    name: 'index',
    component: IndexView,
    redirect: '/home',
    children:[
      {
        path: '/home',
        name: 'home',
        component: () => import('../views/HomeView.vue')
      },
      {
        path: '/notes',
        name: 'notes',
        component: () => import('../views/NotesView.vue'),
        redirect: '/notes/list',
        children: [
          {
            path: '/notes/list',
            name: 'list',
            component: () => import('../views/NotesListView.vue')
          },
          {
            path: '/notes/detail',
            name: 'detail',
            component: () => import('../views/NotesDetailView.vue')
          }
        ]
      },
      {
        path: '/mood',
        name: 'mood',
        component: () => import('../views/MoodView.vue')
      }
    ]
  },
  // 管理页面
  {
    path: '/admin',
    name: 'admin',
    component: () => import('../views/AdminView.vue'),
    redirect: '/admin/setting',
    children: [
      {
        path: '/admin/setting',
        name: 'admin-setting',
        component: () => import('../views/AdminWebSettingView.vue')
      },
      {
        path: '/admin/notes/list',
        name: 'admin-notes-list',
        component: () => import('../views/AdminNotesListView.vue')
      },
      {
        path: '/admin/notes/add',
        name: 'admin-notes-add',
        component: () => import('../views/AdminNotesAddView.vue')
      },
      {
        path: '/admin/notes/edit',
        name: 'admin-notes-edit',
        component: () => import('../views/AdminNotesEditView.vue')
      },
      {
        path: '/admin/mood',
        name: 'admin-mood',
        component: () => import('../views/AdminMoodView.vue')
      }
    ]
  },
  // 404
  { path: '/:pathMatch(.*)*',
    name: 'NotFound',
    component: () => import('../views/NotFoundView.vue')
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

// 路由导航卫士
router.beforeEach(async (to, from, next) => {
  const token = window.localStorage.getItem('token') // 验证是否登录
  const adminRoutePath = [
    '/admin',
    '/admin/setting',
    '/admin/notes/list',
    '/admin/notes/add',
    '/admin/notes/edit',
    '/admin/article/list',
    '/admin/article/add',
    '/admin/article/edit',
    '/admin/mood'
  ]

  

  if (adminRoutePath.indexOf(to.path) > 0) {
    if (token) {
      const res = await http.get('/admin/token')
      if (res) {
        next()
      }
      else {
        window.localStorage.removeItem('token')
        next({ path: '/home' })
      }
    } else {
      ElMessage.info('提示：暂无权限访问，请登录')
      next({ path: '/home' })
    }
  } else {
    next()
  }
})

export default router
