import request from './http'

class Article {

  constructor(id, title, category, link, content, isPublish, source, publishTime, picUrl){
    this.id = id
    this.title = title
    this.category = category
    this.source = source
    this.link = link
    this.content = content
    this.isPublish = isPublish
    this.publishTime = publishTime
  }

  // 新增文章
  async insertArticle(){
    const data = {
      id: this.id,
      title: this.title,
      category: this.category,
      source: this.source,
      link: this.link,
      content: this.content,
      isPublish: this.isPublish ? 1 : 0,
      publishTime: this.publishTime
    }
    const res = await request.post('/articles', JSON.stringify(data))
    if (res) return true
    return false
  }

  // 删除文章
  async deleteArticle(){
    const res = await request.delete(`/articles?id=${this.id}`)
    if (res) return true
    return false
  }

  // 更新文章
  async updateArticle(){
    const data = {
      id: this.id,
      title: this.title,
      category: this.category,
      source: this.source,
      link: this.link,
      content: this.content,
      isPublish: this.isPublish,
      publishTime: this.publishTime
    }
    const res = await request.patch('/articles', JSON.stringify(data))
    if (res) return true
    return false
  }

  // 更新文章发布状态
  async changeArticlePublishStatus(){
    const res = await request.patch(`/articles/publish?id=${this.id}&isPublish=${this.isPublish}`)
    if (res) return true
    return false
  }


  // 获取文章
  static async getArticle(type, category, id, offset, limit){
    const res = await request.get(`/articles?type=${type}&category=${category}&id=${id}&offset=${offset}&limit=${limit}`)
    if (res) return res.data
    return null
  }

  // 获取最新文章
  async getNewNotes() {
    const res = await request.get('/articles/new')
    if (res) {
      console.log(res)
      return res.data
    }
  }

  // 根据类别获取文章列表
  async getArticleByCategory() {
    const res = await request.get('/articles/category')
    if (res) {
      console.log(res)
      return res.data
    }
  }
}

export default Article