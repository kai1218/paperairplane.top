import axios from 'axios'
import { ElMessage } from 'element-plus'

// 创建实例
const instance = axios.create({
  baseURL: 'https://paperairplane.top:7777/api',
  // baseURL: 'http://localhost:7777/api',
  timeout: 10000,
  withCredentials: true
})
// 配置POST请求头
instance.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8'
instance.defaults.headers.patch['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8'

// 请求拦截器（主要应用于判断登录cookie）
instance.interceptors.request.use(
  config => {
     /**
      * 登录流程控制中，根据本地是否存在token判断用户的登录情况
      但是即使token存在，也有可能token是过期的，所以在每次的请求头中携带token
      后台根据携带的token判断用户的登录情况，并返回给我们对应的状态码
      而后我们可以在响应拦截器中，根据状态码进行一些统一的操作。
      */
      const token = window.localStorage.getItem('token')
      token && (config.headers.Authorization = token)
      return config
  },
  error => Promise.error(error)
)
// 响应拦截器(处理请求报错)
instance.interceptors.response.use(
  // 请求成功
  // 判断状态码，200放行，其他情形由error处理
  res => res.status === 200 ? Promise.resolve(res) : Promise.reject(res),
  error => {
    const { response } = error
    if (response) { // 有响应
      // ElMessage.error(response.data.error)
    } else { // 无响应
      // console.log('暂无响应，请稍后再试')
      return Promise.reject(error)
    }
  }
)

export default instance
