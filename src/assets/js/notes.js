import request from './http'

class Note {

  constructor(id, title, summary, tags, content, isPublish, isTop, publishTime, picUrl, picMinUrl){
    this.id = id
    this.title = title
    this.summary = summary
    this.tags = tags
    this.content = content
    this.isPublish = isPublish
    this.isTop = isTop
    this.publishTime = publishTime
    this.picUrl = picUrl
    this.picMinUrl = picMinUrl
  }

  // 新增笔记
  async insertNotes(){
    const data = {
      id: this.id,
      title: this.title,
      summary: this.summary,
      tags: this.tags,
      content: this.content,
      isPublish: this.isPublish,
      isTop: this.isTop,
      publishTime: this.publishTime,
      picUrl: this.picUrl,
      picMinUrl: this.picMinUrl
    }
    const res = await request.post('/notes', JSON.stringify(data))
    if (res) return true
    return false
  }

  // 删除笔记
  async deleteNotes(){
    const res = await request.delete(`/notes?id=${this.id}`)
    if (res) return true
    return false
  }

  // 更新笔记
  async updateNotes(){
    const data = {
      id: this.id,
      title: this.title,
      summary: this.summary,
      tags: this.tags,
      content: this.content,
      isPublish: this.isPublish,
      isTop: this.isTop,
      publishTime: this.publishTime,
      picUrl: this.picUrl,
      picMinUrl: this.picMinUrl
    }
    const res = await request.patch('/notes', JSON.stringify(data))
    if (res) return true
    return false
  }

  // 获取笔记详情
  async getNotesContent(){
    const res = await request.get(`/notes/content?id=${this.id}`)
    if (res) {
      console.log(res)
      return res.data
    }
  }

  // 获取笔记
  static async getNotes(typeName, id, tag, offset, limit) {
    const res = await request.get(`/notes?type=${typeName}&id=${id}&tag=${tag}&offset=${offset}&limit=${limit}`)
    if (res) {
      return res.data
    }
  }

  // 修改笔记置顶状态
  async changeNotesTopStatus() {
    const res = await request.patch(`/notes/top?id=${this.id}&isTop=${this.isTop}`)
    if (res) return true
    return false
  }

  // 修改笔记发布状态
  async changeNotesPublishStatus() {
    const res = await request.patch(`/notes/publish?id=${this.id}&isPublish=${this.isPublish}`)
    if (res) return true
    return false
  }

  // 获取笔记标签
  static async getNotesTags() {
    const res = await request.get('/notes/tags')
    if (res) return res.data
  }

  // 根据标签获取笔记列表
  static async getNotesByTag() {
    const res = await request.get('/notes/tags/list')
    if (res) {
      console.log(res)
      return res.data
    }
  }
}

export default Note