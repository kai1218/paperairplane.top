export default (time:number, type:string) => {
  let y = new Date(time).getFullYear()
  let m = new Date(time).getMonth() + 1
  let d = new Date(time).getDate()
  let h = new Date(time).getHours()
  let ms = new Date(time).getMinutes()
  let M = m > 9 ? m : "0" + m
  let D = d > 9 ? d : "0" + d
  let H = h > 9 ? h : "0" + h
  let MS = ms > 9 ? ms : "0" + ms
  if (type == 'date') {
    return `${y}-${M}-${D}`
  } else {
    return `${y}-${M}-${D} ${H}:${MS}`
  }
}