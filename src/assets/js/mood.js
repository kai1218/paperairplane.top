import request from './http'

class Mood {
  constructor(id, content, picUrl, publishTime, tag, username, password){
    this.id = id
    this.content = content
    this.picUrl = picUrl
    this.publishTime = publishTime
    this.tag = tag
    this.username = username
    this.password = password
  }

  async insert(){
    const data = {
      id: this.id,
      content: this.content,
      picUrl: this.picUrl,
      publishTime: this.publishTime,
      tag: this.tag,
      username: this.username,
      password: this.password
    }
    const res = await request.post('/moods', JSON.stringify(data))
    if (res) return true
    return false
  }

  static async select(type, offset, limit){
    const res = await request.get(`/moods?type=${type}&offset=${offset}&limit=${limit}`)
    if (res) return res.data
    return null
  }

  static async delete(id){
    const res = await request.delete(`/moods?id=${id}`)
    if (res) return true
    return false
  }

}

export default Mood