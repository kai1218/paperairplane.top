import request from './http'

const adminRequest = {
  // 登录
  async login(data){
    const res = await request.post('/admin/login', JSON.stringify(data))
    if (res) {
      window.localStorage.setItem('token', res.data.token)
      return true
    } else {
      return false
    }
  },
  // 校验token
  async checkToken(){
    const res = await request.get('/admin/token')
    if (res) return true
    else return false
  },
}

const logoPicUrlRequest = {
  // 获取网站logo地址
  async get(){
    const res = await request.get('/admin/logoPicUrl')
    if (res) {
      return res.data
    }
    return
  },
  // 更新网站logo地址
  async set(path){
    const data = { 'logoPicUrl': path }
    const res = await request.patch('/admin/logoPicUrl', JSON.stringify(data))
    if (res) return true
    else return false
  }
}

const userPicUrlRequest = {
  // 获取站长头像
  async get(){
    const res = await request.get('/admin/userPicUrl')
    if (res) {
      return res.data
    }
  },
  // 更新站长头像
  async set(path){
    const data = { 'userPicUrl': path }
    const res = await request.patch('/admin/userPicUrl', JSON.stringify(data))
    if (res) return true
    else return false
  }
}

const userInfoRequest = {
  // 获取站长信息
  async get(){
    const res = await request.get('/admin/userInfo')
    if (res) {
      return res.data
    }
  },
  // 更新站长信息
  async set(data){
    const res = await request.patch('/admin/userInfo', JSON.stringify(data))
    if (res) return true
    else return false
  }
}

const noticeRequest = {
  // 获取网站公告
  async get(){
    const res = await request.get('/admin/notice')
    if (res) {
      return res.data
    }
  },
  // 更新网站公告
  async set(txt){
    const data = { 'notice': txt }
    const res = await request.patch('/admin/notice', JSON.stringify(data))
    if (res) return true
    else return false
  }
}

// 获取数量
const countRequest = {
  async get(){
    const res = await request.get('/admin/count')
    if (res) return res.data
  }
}

// 获取新闻
const newRequest = {
  async get(){
    const res = await request.get('/news')
    if (res) return res.data
  }
}

export { logoPicUrlRequest, userPicUrlRequest, userInfoRequest ,noticeRequest, countRequest, adminRequest, newRequest }