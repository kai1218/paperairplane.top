import { createStore } from 'vuex'

export default createStore({
  state: {
    id: '',
    articleID: '',
    category: '全部类别'
  },
  getters: {
  },
  mutations: {
    setId(state, data){
      state.id = data
    },
    setArticleID(state, data){
      state.articleID = data
    },
    setCategory(state, data){
      state.category = data
    }
  },
  actions: {
  }
})
