import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './assets/less/style.less'
import 'element-plus/theme-chalk/el-message.css'
import 'element-plus/es/components/message-box/style/css'

createApp(App).use(store).use(router).mount('#app')
